import { createI18n } from 'vue-i18n';
import en from './locales/en';
import zh from './locales/zh';

const messages = {
    zh,
    en
}
const localLang = navigator.language.split('-')[0];
const storageLang = window.localStorage.getItem('locale')?.split('"')[1].split('"')[0].toLocaleLowerCase() || 'zh';
// const c = (storageLang.toLocaleLowerCase() !== 'zh' && storageLang.toLocaleLowerCase() !== 'en') ? 'zh' : storageLang;
const i18n = createI18n({
    globalInjection: true, //全局生效$t
    locale: storageLang || localLang || 'zh',
    messages,
    legacy: false,
})
// i18n.locale = 'zh'

export default i18n