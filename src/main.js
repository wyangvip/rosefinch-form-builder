import { createApp } from 'vue'
import App from './App.vue'
// import './assets/fonts/iconfont.css';
// import './assets/fonts/iconfont.js';
// import i18n from './i18n/index'

const app = createApp(App)
    // .use(i18n)

app.mount('#app')
