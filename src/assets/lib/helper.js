/**
 * 移动元素到指定目标元素位置
 * @param {*} array 
 * @param {*} src 拖动源
 * @param {*} target 放置目标
 */
export function move(array, src, target) {
    for (var index in array) {
        // 插入到target根元素位置
        if (array[index].index == target.index) {
            console.log(array[index], src, target)
            array.splice(index, 0, src);
            return;
        }
        // 遍历子节点
        if (array[index].children) {
            move(array[index].children, src, target);
        }
    }
}

/**
 * 删除元素
 * @param {*} array 
 * @param {*} src 
 * @returns 
 */
export function remove(array, src) {
    for (var index in array) {
        // 删除
        if (array[index].index == src.index) {
            array.splice(index, 1);
            return;
        }
        // 遍历子节点
        if (array[index].children) {
            remove(array[index].children, src);
        }
    }
}

/**
 * 更新
 * @param {*} array 
 * @param {*} src 
 * @returns 
 */
export function update(array, src) {
    for (var index in array) {
        // 替换节点
        if (array[index].index == src.index) {
            array[index] = src;
            return;
        }
        // 遍历子节点
        if (array[index].children) {
            update(array[index].children, src);
        }
    }
}