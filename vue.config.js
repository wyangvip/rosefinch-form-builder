const { defineConfig } = require('@vue/cli-service')

module.exports = defineConfig({
  transpileDependencies: true,
  publicPath: "./",
  // chainWebpack: config => {
  //   config.module
  //     .rule('svg')
  //     .use('file-loader')
  //     .loader('file-loader')
  //     .tap(options => {
  //       // 修改它的选项...
  //       return options
  //     })
  // }
  // configureWebpack: {
  //   module: {
  //     rules: [
  //       {
  //         test: /\.(ttf|otf|woff|woff2|svg)$/,
  //         use: [
  //           {
  //             loader: 'file-loader',
  //             options: {
  //               name: '[name].[ext]',
  //               outputPath: 'fonts/',
  //             },
  //           },
  //         ],
  //       },
  //     ],
  //   },
  // },
  // chainWebpack: config => {
  //   const svgRule = config.module.rule('svg')
  //   // 清除已有的所有 loader。
  //   // 如果你不这样做，接下来的 loader 会附加在该规则现有的 loader 之后。
  //   svgRule.uses.clear()
  //   // 添加要替换的 loader
  //   svgRule
  //     .use('vue-svg-loader')
  //     .loader('vue-svg-loader')
  // }
  // chainWebpack(config) {
  //   // 处理字体文件
  //   config.module
  //     .rule('fonts')
  //     .test(/\.(png|woff|woff2|eot|ttf|svg)$/)
  //     .use('url-loader')
  //     .loader('url-loader')
  //     .tap(options => {
  //       return {
  //         ...options,
  //         limit: 1000000, // 根据您的需求设置适当的 limit 值
  //         mimetype: 'application/font-woff2',
  //         name: 'fonts/[name].[hash:8].[ext]', // 根据您的需求设置适当的文件名及路径
  //       };
  //     });
  // },
})
