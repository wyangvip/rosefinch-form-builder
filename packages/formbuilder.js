import FormBuilder from '../src/components/FormBuilder.vue'

// 给组件定义install方法
FormBuilder.install = Vue => {
  Vue.component(FormBuilder.name, FormBuilder)
}

export default FormBuilder