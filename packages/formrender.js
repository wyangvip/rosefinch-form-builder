import FormRender from '../src/components/FormRender.vue'

// 给组件定义install方法
FormRender.install = Vue => {
  Vue.component(FormRender.name, FormRender)
}

export default FormRender