# rosefinch-form-builder

#### 介绍
网上找了很多表单生成器，试用之后发现还是不太合适，最后决定自己写一个。rosefinch-from-builder是一个vue3的原生表单构建器，现在还存在一些问题，后面有空会不断完善。
![](./images/preview.png)

* 支持拖拽调整布局
* 支持正则验证
* 支持导出json/html
* 支持自定义toolbar
* 支持多语言（需要自己完善一下）
* 二次开发上手容易，文档下面有二次开发的教程

#### 待完善

* iconfonts需要手动添加到项目目录（还没找到好的解决方案）：src/assets/fonts/

#### 下载和安装

因npm仓库没有注册成功，所以没发布版本到npm仓库，只是发布到了本地:

```
git clone --depth=1 https://gitee.com/wyangvip/rosefinch-form-builder.git rosefinch-form-builder
npx verdaccio
# 发布过程参考二次开发
npm install rosefinch-form-builder --registry=http://localhost:4873
```

#### 使用说明

1.  修改main.js/main.ts
```
import FormBuilder from "rosefinch-form-builder";
const app = createApp(App)
app.use(FormBuilder)
```
2.  页面中使用
```
<template>
    <form-builder style="margin: 10px" />
</template>
<style>
@import "rosefinch-form-builder/rosefinch-form-builder.css";
</style>
```

#### 方法
待完善
```
# FormBuilder
    exportJson()
    exportHTML()
    changeLang(lang)//lang:'zh'或'en'
# FormRender
    getFormData()
    getSourceJson()
    getForm()
```

#### 插槽
```
  <form-builder>
    <template v-slot:toolbar>
      <lay-button type="primary" @click="onPublishForm">发布</lay-button>
    </template>
  </form-builder>
```

#### 二次开发

如果想进行二次开发，这些可能对你有帮助：
1.  Elements.vue中可以增加自己的标签
2.  Properties.vue中可以添加和修改属性
3.  发布：
```
# 在根目录中执行命令
npm run package

cp packages/package.json dist/
cp README.md dist/
cd dist/
# 修改package.json里面版本号和仓库信息

npm publish
```
